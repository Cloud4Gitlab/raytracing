# Raytracing CPU & GPU
Ce projet est réalisé sous GKit dans le cadre du cours de rendu sur cartes graphiques, par Jean Claude IEHL, 
de l'université de Lyon 1. <br>
Attention, il s'agit d'un projet en cours, de ce fait il se peut que le code ne soit pas encore clean ou optimisé ! <br>
Le code du projet est contenu dans le dossier "/src/main/".

# Installation
Pour cette partie, suivre les instructions données dans le fichier "install.txt", <br/>
il indique les librairies à télécharger et inclures pour le bon fonctionnement de ce projet.

# Lancement
## Première fois
Juste après l'installation du projet.

```bash
 -$> premake4 gmake
```

## A chaque fois
Pour compiler le programme principal.
```bash
 -$> make raytracing
```

Pour compiler le reader pour le fichier .hdr, que l'application produira à chaque lancement :
```bash
 -$> make image_viewer
```

# Test
Afin de tester l'application, taper la simple commande suivante :

```bash
 -$> ./bin/raytracing [CPU | GPU]
```

CPU correspondant à la valeur 0 <br/>
GPU correspondant à toutes autres valeurs.

(Pour finir l'application faire crtl-c ou entrez un chiffre)

Cette application produira deux images une en png où l'on n'y vois pas grand chose car ce n'est que le calcul de la lumiere globale. <br />
Et un .hdr lisible via l'application image-viewer (voir ci-dessous) afin de voir plus en detail le resultat. <br/>

```bash
 -$> ./bin/image-viewer monFichier.hdr
```
