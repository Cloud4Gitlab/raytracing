#version 430

#ifdef COMPUTE_SHADER

#define FLT_MAX 3.402823466e+38 // pour GPU
#define M_PI	3.14159265358979323846	/* pi */

//--- STRUCTURES SECTION ---//

struct Triangle{
	int id; 	//triangle id
	vec3 p; 	//sommet, 
	vec3 e1;	// arete ab
	vec3 e2; 	// arete ac
}; //octets 48

struct Ray{
	float tmax;
	vec3 o;
	vec3 d;
};

struct PN_POINT{
	vec3 p;
	vec3 pn;
	vec3 pn001;
};

struct World {
	vec3 t;
	vec3 b;
	vec3 n;
};

//--- STORAGE BUFFER SECTION ---//
layout(binding=0, std430) readonly buffer triangleData{
	Triangle triangles[];
};

layout(binding=3, std430) buffer pointData{
	PN_POINT points[];
};

layout(binding=4, std430) buffer sumLightData{
	float sumLights[];
};

//--- UNIFORM SECTION ---//
uniform int N;
uniform int triangles_count;
uniform int Dir_number;
uniform int scale;
uniform float f_r;
uniform float phi;

//--- FUNCTIONS SECTION ---//
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

/** Construct a world */
World world_creation(vec3 n){
	World h;
	float signe = sign(n.z);
	float a = -1.0 / (signe + n.z);
	float d = n.x * n.y * a;
	h.n = n;
	h.t = vec3(1.0 + signe * n.x * n.x * a, signe * d, -signe * n.x);
	h.b = vec3(d, signe + n.y * n.y * a, -n.y);
	return h;
}

/** transform le vecteur du repere local vers le repere du monde */
vec3 world_operator(vec3 local, World w){
	return vec3(local.x * w.t + local.y * w.b + local.z * w.n);
}

/** Sert à definir une direction */
vec3 fifi(int i, int n, float pert, World w){
	float cosTheta = 1.0 - ((2.0 * i + 1.0) / (2.0 * n));
	float sinTheta = sqrt(1.0 - (cosTheta * cosTheta));

	float phi_i = (2.0 * M_PI) * ((i / phi) + pert - floor((i / phi) + pert));

	return world_operator(vec3(cos(phi_i) * sinTheta, sin(phi_i) * sinTheta, cosTheta), w);
}

/** World.n = n */
float pdf(vec3 v, vec3 n) { 
	if (dot(v, n) < 0.0) return 0.0; 
	else return 1.0; 
}

/** Test d'intersection entre un rayon et un triangle */
bool intersect(const Ray ray, const Triangle tri, float tmax){
	vec3 pvec = cross(ray.d, tri.e2);
    float det = dot(tri.e1, pvec);
        
    float inv_det = 1 / det;
    vec3 tvec = ray.o - tri.p;

    float u= dot(tvec, pvec) * inv_det;
    if(u < 0 || u > 1) { return false; }

    vec3 qvec= cross(tvec, tri.e1);
    float v= dot(ray.d, qvec) * inv_det;
    if(v < 0 || u + v > 1){ return false; }

    float t= dot(tri.e2, qvec) * inv_det;
    if(t > tmax || t < 0){ return false; }

	return true;
}

/** Test le si le rayon est visible ou non */
bool visible(Ray ray){
	for (int i = 0; i < triangles_count; ++i)
		if (intersect(ray, triangles[i] ,ray.tmax))
			return false;

	return true;
}

/** self explanatory */
Ray createRay(vec3 o, vec3 e){
	Ray r;
	r.tmax = 1;
	r.o = o;
	r.d = e - o;

	return r;
}

layout(local_size_x = 1024) in;
void main(){
    uint id = gl_GlobalInvocationID.x;

    if(id < N){
		float currentSumLight = 0.0;
		
		PN_POINT point = points[id];

		World world;
		world = world_creation(point.pn);

		float pert = rand(vec2(id, point.pn.z));

        for(int i = 0; i < Dir_number; i++){
			vec3 w = fifi(i, Dir_number, pert, world);
			normalize(w);

			Ray shadow = createRay(point.pn001, point.p + w * scale);

			if(visible(shadow)){
				float theta = max(dot(point.pn, w), 0);
				float pdf_value = pdf(w, world.n);
				currentSumLight = currentSumLight + (theta / pdf_value);
			}
        }
		sumLights[id] = f_r * currentSumLight;
    }
}

#endif

// ! WORK IN PROGRESS ! //