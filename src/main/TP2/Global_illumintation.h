#pragma once

#include "vec.h"

#include <cstdio>
#include <algorithm>

const float phi = (sqrt(5) + 1.0f) / 2.0f;

// construit un repere ortho tbn, a partir d'un seul vecteur, la normale / axe z...
// cf "generating a consistently oriented tangent space" 
// http://people.compute.dtu.dk/jerf/papers/abstracts/onb.html
// cf "Building an Orthonormal Basis, Revisited", Pixar, 2017
// http://jcgt.org/published/0006/01/01/
struct World {
	World(const Vector& _n) : n(_n) {
		float sign = std::copysign(1.0f, n.z);
		float a = -1.0f / (sign + n.z);
		float d = n.x * n.y * a;
		t = Vector(1.0f + sign * n.x * n.x * a, sign * d, -sign * n.x);
		b = Vector(d, sign + n.y * n.y * a, -n.y);
	}

	// transforme le vecteur du repere local vers le repere du monde
	Vector operator( ) (const Vector& local)  const { 
		return local.x * t + local.y * b + local.z * n; 
	}

	// transforme le vecteur du repere du monde vers le repere local
	Vector inverse(const Vector& global) const { return Vector(dot(global, t), dot(global, b), dot(global, n)); }

	Vector t;
	Vector b;
	Vector n;
};


// genere une direction uniforme 1 / 2pi, cf GI compendium, eq 34
struct UniformDirection
{
	UniformDirection(const int _n, const Vector& _z) : world(_z), n(_n) {}

	Vector operator() (const float u1, const float u2) const
	{
		float cos_theta = u1;
		float phi = 2.f * float(M_PI) * u2;
		float sin_theta = std::sqrt(std::max(0.f, 1.f - cos_theta * cos_theta));

		return world(Vector(std::cos(phi) * sin_theta, std::sin(phi) * sin_theta, cos_theta));
	}

	float pdf(const Vector& v) const { if (dot(v, world.n) < 0) return 0; else return 1.f / (2.f * float(M_PI)); }
	int size() const { return n; }

protected:
	World world;
	int n;
};

struct fifi //version perturbé
{
	fifi(const int _n, const Vector& _z, const float perturbation) : world(_z), n(_n), pert(perturbation) {}


	Vector operator() (const int i) const {
		float cosTheta = 1.0f - (float(2 * i + 1) / (float)(2 * n));
		float sinTheta = sqrt(1.0f - (cosTheta * cosTheta));

		float phi_i = (2.0f * M_PI) * ((float(i) / phi) + pert - floor((float(i) / phi) + pert));

		return world(Vector(std::cos(phi_i) * sinTheta, std::sin(phi_i) * sinTheta, cosTheta));
	}

	float pdf(const Vector& v) const { if (dot(v, world.n) < 0) return 0; else return 1.0f; }
	int size() const { return n; }

protected:
	World world;
	int n;
	float pert;
};

