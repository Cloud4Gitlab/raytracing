#version 430

#ifdef COMPUTE_SHADER

#define FLT_MAX 3.402823466e+38 // pour GPU
#define M_PI	3.14159265358979323846	/* pi */

//--- STRUCTURES SECTION ---//

struct Triangle{
	int id; 	//triangle id
	vec3 p; 	//sommet, 
	vec3 e1;	// arete ab
	vec3 e2; 	// arete ac
}; //octets 48

struct Ray{
	float tmax;
	vec3 o;
	vec3 d;
};

struct Hit{
	int triangle_id;
	float t;
	float u, v;
};

struct World {
	vec3 t;
	vec3 b;
	vec3 n;
};

struct TriangleData{
    vec3 a, b, c;
    vec3 na, nb, nc;
};

//--- STORAGE BUFFER SECTION ---//
layout(binding=0, std430) readonly buffer triangleData{
	Triangle triangles[];
};

layout(binding=1, std430) readonly buffer triangleDataData{ //just love repetition
	TriangleData trianglesData[];
};

layout(binding=2, std430) buffer rayData{
	Ray rays[];
};

layout(binding=3, std430) buffer hitData{
	Hit hits[]; //output
};

layout(binding=4, std430) buffer sumLightData{
	float sumLights[];
};

//--- UNIFORM SECTION ---//
uniform int N;
uniform int triangles_count;
uniform int Dir_number;
uniform int scale;
uniform float f_r;
uniform float phi;

//--- FUNCTIONS SECTION ---//
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

/** Construct a world */
World world_creation(vec3 n){
	World h;
	float signe = sign(n.z);
	float a = -1.0 / (signe + n.z);
	float d = n.x * n.y * a;
	h.n = n;
	h.t = vec3(1.0 + signe * n.x * n.x * a, signe * d, -signe * n.x);
	h.b = vec3(d, signe + n.y * n.y * a, -n.y);
	return h;
}

/** transform le vecteur du repere local vers le repere du monde */
vec3 world_operator(vec3 local, World w){
	return vec3(local.x * w.t + local.y * w.b + local.z * w.n);
}

/** Sert à definir une direction */
vec3 fifi(int i, int n, float pert, World w){
	float cosTheta = 1.0 - ((2.0 * i + 1.0) / (2.0 * n));
	float sinTheta = sqrt(1.0 - (cosTheta * cosTheta));

	float phi_i = (2.0 * M_PI) * ((i / phi) + pert - floor((i / phi) + pert));

	return world_operator(vec3(cos(phi_i) * sinTheta, sin(phi_i) * sinTheta, cosTheta), w);
}

/** World.n = n */
float pdf(vec3 v, vec3 n) { 
	if (dot(v, n) < 0.0) return 0.0; 
	else return 1.0; 
}

/** Test d'intersection entre un rayon et un triangle */
bool intersect(const Ray ray, const Triangle tri, float tmax){
	vec3 pvec = cross(ray.d, tri.e2);
    float det = dot(tri.e1, pvec);
        
    float inv_det = 1 / det;
    vec3 tvec = ray.o - tri.p;

    float u= dot(tvec, pvec) * inv_det;
    if(u < 0 || u > 1) { return false; }

    vec3 qvec= cross(tvec, tri.e1);
    float v= dot(ray.d, qvec) * inv_det;
    if(v < 0 || u + v > 1){ return false; }

    float t= dot(tri.e2, qvec) * inv_det;
    if(t > tmax || t < 0){ return false; }

	return true;
}

/** Test le si le rayon est visible ou non */
bool visible(Ray ray){
	for (int i = 0; i < triangles_count; ++i)
		if (intersect(ray, triangles[i] ,ray.tmax))
			return false;

	return true;
}

/** self explanatory */
Ray createRay(vec3 o, vec3 e){
	Ray r;
	r.tmax = 1;
	r.o = o;
	r.d = e - o;

	return r;
}

//algo testé et fonctionnel en C++
bool intersect(const Ray ray, const Triangle tri, Hit hot, out Hit hit){
	hit = hot;

	vec3 pvec = cross(ray.d, tri.e2);
    float det = dot(tri.e1, pvec);
        
    float inv_det = 1 / det;
    vec3 tvec = ray.o - tri.p;

    float u= dot(tvec, pvec) * inv_det;
    if(u < 0 || u > 1) { return false; }

    vec3 qvec= cross(tvec, tri.e1);
    float v= dot(ray.d, qvec) * inv_det;
    if(v < 0 || u + v > 1){ return false; }

    float t= dot(tri.e2, qvec) * inv_det;
    if(t > hot.t || t < 0){ return false; }

	hit.t = t;
	hit.u = u;
	hit.v = v;
	hit.triangle_id = tri.id;

	return true;
}


/** Get the normal of the triangle hit */
vec3 normal_h_tt(Hit hit, TriangleData triangle){ 
    return normalize((1.0 - hit.u - hit.v) * (triangle.na) + hit.u * (triangle.nb) + hit.v * (triangle.nc)); 
}

/** Used to create a point from a hit and it's ray*/
vec3 point_h_r(Hit h, Ray r){
    vec3 v = vec3(h.t * r.d.x, h.t * r.d.y, h.t * r.d.z);
    return vec3(r.o.x + v.x, r.o.y + v.y, r.o.z + v.z);
}

/** Used to create a point from a hit and the hitted triangle data*/
/*vec3 point_h_tt(Hit h, TriangleData triangle){
    return (1.0 - h.u - h.v) * triangle.a + h.u * triangle.b + h.v * triangle.c;
}*/

layout(local_size_x = 1024) in;
void main(){
	uint id = gl_GlobalInvocationID.x;

	bool has_intersect = false;
	if(id < N){
		hits[id].triangle_id = -1;
		hits[id].t = FLT_MAX;
		hits[id].u = 0;
		hits[id].v = 0;
		Hit hot = hits[id];
		
        for(int i = 0; i < triangles_count; i++){
			if(intersect(rays[id], triangles[i], hot , hits[id])){
				has_intersect = true;
				hot = hits[id];
			}
		}

        if(has_intersect){
            TriangleData tri = trianglesData[hits[id].triangle_id];
            
            vec3 p = point_h_r(hits[id], rays[id]);
            vec3 pn = normal_h_tt(hits[id], tri);

            if (dot(pn, rays[id].d) > 0)// retourne la normale vers l'origine du rayon
					pn = -pn;

            vec3 pn001 = p + pn * 0.001;

            float currentSumLight = 0.0;

            World world;
            world = world_creation(pn);

            float pert = rand(vec2(id, pn.z));

            for(int i = 0; i < Dir_number; i++){
                vec3 w = fifi(i, Dir_number, pert, world);
                normalize(w);

                Ray shadow = createRay(pn001, p + w * scale);

                if(visible(shadow)){
                    float theta = max(dot(pn, w), 0);
                    float pdf_value = pdf(w, world.n);
                    currentSumLight = currentSumLight + (theta / pdf_value);
                }
            }
            sumLights[id] = f_r* currentSumLight;
        }else{
            sumLights[id] = 0.5;
        }

	}
		
}

#endif

// ! WORK IN PROGRESS ! //