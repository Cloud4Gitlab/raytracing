#include <cfloat>
#include <random>
#include <chrono>
#include <omp.h>

#include "app_time.h"

#include "vec.h"
#include "mat.h"

#include "program.h"
#include "uniforms.h"
#include "texture.h"
#include "mesh.h"

#include "wavefront.h"
#include "orbiter.h"
#include "atomic"

#include "Global_illumintation.h" 

#include "image.h"
#include "image_io.h"
#include "image_hdr.h"

#define DIR_NUMBER 256


class Raytrace_Compute : public AppTime {
public:
	
	/** Default Constructor */
	Raytrace_Compute() : AppTime(1024, 640, 4, 3) {}

	/** Constructor used to setup the BVH and the camera */
	Raytrace_Compute(GBVH &_bvh, Orbiter &_camera, Mesh &_mesh) : 
		AppTime(1024, 640, 4, 3),
		currentScene(_bvh), camera(_camera), mesh(_mesh) {}

	struct PN_POINT{
		glsl::vec3 p;
		glsl::vec3 pn;
		glsl::vec3 pn001;
	};

	/** Used to initiate and generate the buffers */
	int init() {
		ray_size = pixelNumber = window_height() * window_width();
		
		// STEP 0 : sending triangle and BVH (if there is one) to the shader
		ray_list.resize(pixelNumber);
		screen_coord.resize(pixelNumber);
		points.resize(pixelNumber);

		/** Static read only triangles buffer */
		glGenBuffers(1, &triangles_buffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, triangles_buffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(BTriangle) * currentScene.triangles.size(), currentScene.triangles.data(), GL_STATIC_READ);

		/** in/out ray buffer */
		glGenBuffers(1, &ray_buffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ray_buffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(BRay) * ray_list.size(), ray_list.data(), GL_DYNAMIC_COPY);

		/** Out buffer */
		glGenBuffers(1, &hit_buffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, hit_buffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(BHit) * ray_list.size(), nullptr, GL_DYNAMIC_COPY);

		/** in/out ray buffer */
		glGenBuffers(1, &points_buffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, points_buffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(PN_POINT) * points.size(), points.data(), GL_DYNAMIC_COPY);

		/** Out buffer */
		glGenBuffers(1, &sumLight_buffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, sumLight_buffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * points.size(), nullptr, GL_DYNAMIC_COPY);

		program = read_program(program_str);
		program_print_errors(program);

		program_bounce_ray = read_program(program_bounce_ray_str);
		program_print_errors(program_bounce_ray);

		return 1;
	}

	/** Call at the end of the application, after render() */
	int quit() {
		release_program(program);
		release_program(program_bounce_ray);

		glDeleteBuffers(1, &triangles_buffer);
		glDeleteBuffers(1, &ray_buffer);
		glDeleteBuffers(1, &hit_buffer);
		glDeleteBuffers(1, &points_buffer);
		glDeleteBuffers(1, &sumLight_buffer);

		return 1;
	}

	/** Display the properties of the GPU */
	void gpu_properties() {
		// GPU Properties
		GLint thread_max = 0;
		glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &thread_max);

		GLint groups_max[3] = {};//max number of groups that may be dispatched to a compute shader, for the dimentions x, y and z
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &groups_max[0]); // x
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &groups_max[1]); // y
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &groups_max[2]); // z

		std::cout << "Max GPU threads : " << thread_max << std::endl;
		std::cout << "for x: " << groups_max[0] << " y: " << groups_max[1] << " z: " << groups_max[2] << std::endl;

		// taille de la memoire partagee
		GLint size = 0;
		glGetIntegerv(GL_MAX_COMPUTE_SHARED_MEMORY_SIZE, &size);
		printf("shared memory %dKb\n", size / 1024);
		printf("\n");
	}

	/** Calculate the rays shoot from the camera */
	void calculateSceneRay(const Point o, const Point d1, const Vector dx1, const Vector dy1){
		//Precaculate the ray table
		#pragma omp parallel for schedule(dynamic, 16) num_threads(12)
		for(int py = 0; py < window_height(); ++py){
			float y = py + .5f;
			for(int px = 0; px < window_width(); ++px){
				float x = px + .5f;
				Point e = d1 + x * dx1 + y * dy1;  // extremite, see the desc of the frame function in orbiter, do not use if using demi-droite
				ray_list[px + py*window_width()] =  BRay(o, e);
			}
		}
		
	}

	/** 
	 * Used to call the compute shader in order to make intersection tests,
	 * from a given list of Rays
	*/
	std::vector<BHit> compute_ray(const std::vector<BRay> &rays) {
		glUseProgram(program);

		N = ray_size;

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, ray_buffer);
			glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(BRay) * ray_size, ray_list.data());

		program_uniform(program, "N", N);
		program_uniform(program, "triangles_count", (int)currentScene.triangles.size() );
		std::cout << "triangle count : " << (int)currentScene.triangles.size() << std::endl;

		glDispatchCompute((N+1024) / 1024, 1, 1);

		// attendre que les resultats temporaires sont dispos
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		//Recupere les resultats temp
		glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);

		std::vector<BHit> hits(ray_size, BHit());
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, hit_buffer);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(BHit) * hits.size(), hits.data());

		glUseProgram(0);
		return hits;
	}

	std::atomic<int> ite;

	/** Reduce the hit list by removing every hit where the triangle_id is -1 */
	void reduce(const std::vector<BHit>& hit_list){
		ite = 0;

		//#pragma omp parallel for schedule(dynamic, 16) num_threads(12)
		for(int py = 0; py < window_height(); ++py)
			for(int px = 0; px < window_width(); ++px){
				//reduce here
				int i = px + (py*window_width());
				if(hit_list[i].triangle_id != -1){

					PN_POINT pointy;
					// recupere les donnees sur l'intersection
					TriangleData triangle = mesh.triangle(hit_list[i].triangle_id);
					Point	p = point(hit_list[i], ray_list[i]);       // point d'intersection
					Vector	pn = normal(hit_list[i], triangle); // normale interpolee du triangle au point d'intersection
					if (dot(pn, Vector(ray_list[i].d.x, ray_list[i].d.y, ray_list[i].d.z)) > 0)              // retourne la normale vers l'origine du rayon
						pn = -pn;

					pointy.p = p;
					pointy.pn = pn;
					pointy.pn001 = p + pn * .001f;

					points[ite] = pointy;
					screen_coord[ite] = vec2(px, py);
					ite++;
				}
			}
		
		N = ite;
		std::cout << "number" << ite << std::endl;
	}

	/** */
	std::vector<float> calculateLocalRay(){
		glUseProgram(program_bounce_ray);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, points_buffer);
		glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(PN_POINT) * points.size(), points.data());

		program_uniform(program_bounce_ray, "N", N);
		program_uniform(program_bounce_ray, "triangles_count", (int)currentScene.triangles.size() );
		program_uniform(program_bounce_ray, "Dir_number", (int)DIR_NUMBER);
		program_uniform(program_bounce_ray, "scale", (int)scale);
		program_uniform(program_bounce_ray, "f_r", (float) f_r);
		program_uniform(program_bounce_ray, "phi", (float) phi);

		glDispatchCompute((N+1024) / 1024, 1, 1);

		// attendre que les resultats temporaires sont dispos
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		//Recupere les resultats temp
		glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);

		std::vector<float> sumLight(ray_size, float());
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, sumLight_buffer);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float) * sumLight.size(), sumLight.data());
		
		glUseProgram(0);

		return sumLight;
	}

	int render() {
		auto gpu_start = std::chrono::high_resolution_clock::now();
		
		// Parameters Section //
		Point o = camera.position();  // origin			
		Point d1;
		Vector dx1, dy1;

		image = Image(window_width(), window_height(), Black());
		camera.frame(image.width(), image.height(), 1, 45, d1, dx1, dy1);
		
		// STEP 1
		std::cout << "Step 1" << std::endl;
		calculateSceneRay(o, d1, dx1, dy1);

		// STEP 2
		std::cout << "Step 2" << std::endl;
		hit_list = compute_ray(ray_list);
		
		//STEP 3
		std::cout << "Step 3" << std::endl;
		reduce(hit_list);

		//STEP 4
		std::cout << "Step 4" << std::endl;
		std::vector<float> sumLight = calculateLocalRay();
		

		// STEP 5 : IMAGE CONSTRUCTION
		std::cout << "Step 5" << std::endl;
		#pragma omp parallel for schedule(dynamic, 16) num_threads(12)
		for(int i = 0; i < N; ++i){ 
			image(screen_coord[i].x, screen_coord[i].y) = Color(mesh.triangle_material(hit_list[screen_coord[i].x + (screen_coord[i].y * window_width())].triangle_id).diffuse * sumLight[i], 1 );
		}

		auto gpu_stop = std::chrono::high_resolution_clock::now();
		int gpu_time = std::chrono::duration_cast<std::chrono::milliseconds>(gpu_stop - gpu_start).count();
		printf("gpu  %ds %03dms\n", int(gpu_time / 1000), int(gpu_time % 1000));
	

		return -1; //quit the app after one iteration of the render loop
	}

	/** Used to save the current frame */
	void save_frame(){
		
		std::cout << "fin du calcul, debut d'enregistrement :" << std::endl;

		std::string str = "renderFiboPertuGPU";
		str.append(std::to_string(DIR_NUMBER));
		
		std::string str_hdr(str);
		str.append(".png");
		str_hdr.append(".hdr");
		
		write_image(image, str.c_str());
		write_image_hdr(image, str_hdr.c_str()); // � lire dans l'appli fais par le proff de hdr viewer (pas celui en ligne gros)
		
		int dd;
		std::cout << "fin de l'enregistrement" << std::endl;
		std::cin >> dd;

	}


protected:

	
	const float phi = (sqrt(5) + 1.0f) / 2.0f;
	const float f_r = (1.0f / (float)DIR_NUMBER) * (1 / M_PI);
	
	GLuint program;
	GLuint program_bounce_ray;
	
	size_t pixelNumber; //max size buffer for rays and hits
	
	size_t ray_size;

	const float scale = 10;

	Image image;
	
	GBVH currentScene;
	Orbiter camera;
	Mesh mesh;

	std::vector<BRay> ray_list;
	std::vector<BHit> hit_list;

	GLuint triangles_buffer;
	GLuint ray_buffer;
	GLuint hit_buffer;
	GLuint points_buffer;
	GLuint sumLight_buffer;

	std::vector<vec2> screen_coord;
	std::vector<PN_POINT> points;
	
	int N;
	const char* program_str = "src/main/computeRay.glsl";
	const char* program_bounce_ray_str = "src/main/TP2/ComputeLocalRay.glsl";
};