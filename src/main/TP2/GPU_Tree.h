#pragma once

#include <cstdio>
#include <cstdlib>
#include <vector>

#include "vec.h"

#include "orbiter.h"

#include "image.h"
#include "image_io.h"
#include "image_hdr.h"

#include "tuto_bvh_simple.h"

/** 
 * Structure used with the BVH, GPU like version
*/

namespace glsl 
{   
    template < typename T >
    struct alignas(16) gvec3
    {
        alignas(4) T x, y, z;
        
        gvec3( ) {}
		gvec3( T _x, T _y, T _z) : x(_x), y(_y), z(_z) {}
        gvec3( const vec3& v ) : x(v.x), y(v.y), z(v.z) {}
        gvec3( const Point& v ) : x(v.x), y(v.y), z(v.z) {}
        gvec3( const Vector& v ) : x(v.x), y(v.y), z(v.z) {}

		float dot(  const gvec3 v ){
			return x * v.x + y * v.y + z * v.z;
		}
	};
    
    typedef gvec3<float> vec3;
    typedef gvec3<int> ivec3;
    typedef gvec3<unsigned int> uvec3;
    typedef gvec3<int> bvec3;
}

struct BRay{
	float tmax;
	glsl::vec3 o;
	glsl::vec3 d;

	BRay() : tmax(1), o(vec3()), d(vec3()) {}
	BRay( const Point& _o, const Point& _e ) : tmax(1), o(_o), d(Vector(_o, _e))  {}
    BRay( const Point& _o, const Vector& _d ) : tmax(FLT_MAX), o(_o), d(_d)  {}
};

struct BHit{
	int triangle_id;
	float t;
	float u, v;
   
    BHit( ) {
		triangle_id = -1;
		t = 0;
		u = 0;
		v = 0; 
	} // pas d'intersection

    BHit( const int _id, const float _t, const float _u, const float _v ) {
		triangle_id = _id; 
		t = _t;
		u = _u;
		v = _v;
	}    
    operator bool( ) const { return (triangle_id != -1); }      // renvoie vrai si l'intersection est initialisee...

};


struct BNodeHit
{
    float tmin, tmax;
    
    BNodeHit(){
		tmin = 0; 
		tmax = -1; 
	}// pas d'intersection
    
	BNodeHit( const float _tmin, const float _tmax ) {
		tmin = _tmin;
		tmax = _tmax; 
	}
    
    operator bool( ) const { return (tmin <= tmax); } // renvoie vrai si l'intersection est initialisee...
};

struct BNode {
	vec3 m_pMin; //englobante min
	int  m_left;
	vec3 m_pMax; //englobante max
	int	 m_right;

	BNode(const vec3 pMin, const vec3 pMax, const int left, const int right) { 
		m_pMin = pMin;
		m_pMax = pMax; 
		m_left = left; 
		m_right = right; }
/*
	bool leaf() { return m_right < 0; }//-1

	int leaf_begin() {
		assert (leaf());
		return m_left;
	}

	int leaf_end() {
		assert(leaf());
		return -m_right;
	}

	BNodeHit intersect( const BRay ray, const vec3 invd, const float htmax ) const{
        vec3 rmin= m_pMin;
        vec3 rmax= m_pMax;

        if(ray.d.x < 0) std::swap(rmin.x, rmax.x);
        if(ray.d.y < 0) std::swap(rmin.y, rmax.y);
        if(ray.d.z < 0) std::swap(rmin.z, rmax.z);

        vec3 rmin_o = vec3(rmin.x - ray.o.x, rmin.y - ray.o.y, rmin.z - ray.o.z);
		vec3 dmin(rmin_o.x * invd.x, rmin_o.y * invd.y, rmin_o.z * invd.z);
        
		vec3 rmax_o = vec3(rmax.x - ray.o.x, rmax.y - ray.o.y, rmax.z - ray.o.z);
        vec3 dmax(rmax_o.x * invd.x, rmax_o.y * invd.y, rmax_o.z * invd.z);

        float tmin= std::max(dmin.z, std::max(dmin.y, std::max(dmin.x, 0.f)));
        float tmax= std::min(dmax.z, std::min(dmax.y, std::min(dmax.x, htmax)));
        return BNodeHit(tmin, tmax);
    }
*/
};

// utilitaire : renvoie les coordonnees min / max de 2 points / vecteurs CPU only !
/*inline Point min( const Point& a, const Point& b ) { return Point( std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z) ); }
inline Point max( const Point& a, const Point& b ) { return Point( std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z) ); }
*/
inline vec3 min( const vec3 a, const vec3 b ) { return vec3( std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z) ); }
inline vec3 max( const vec3 a, const vec3 b ) { return vec3( std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z) ); }

vec3 ab(const vec3 a, const vec3 b) { return vec3(b.x-a.x, b.y-a.y, b.z-a.z); }

struct BTriangle
{
	int id;
    glsl::vec3 p;
    glsl::vec3 e1;
	glsl::vec3 e2;
    
    BTriangle( const vec3 _p, const vec3 _e1, const vec3 _e2, const int _id ) { 
		p  = _p;
		id = _id;
		e1 = _e1; 
		e2 = _e2; 
	}

};

struct BTriangleData{
	glsl::vec3 a, b, c;
	glsl::vec3 na, nb, nc;

	BTriangleData( const vec3 _na, const vec3 _nb, const vec3 _nc ){
		na = _na;
		nb = _nb;
		nc = _nc;
	}

	BTriangleData( const TriangleData &t ){
		a = t.a;
		b = t.b;
		c = t.c;
		na = t.na;
		nb = t.nb;
		nc = t.nc;
	}
};

/** GPU BVH */
struct GBVH {
	std::vector<BNode> nodes;
	std::vector<BTriangle> triangles;

	GBVH(){}

	BHit intersect(const Ray ray) const {
		BHit hit;
		return hit;
	}
};

/** Used to transform the CPU BVH version into the BVH version of the GPU */
GBVH transform_bvh_to_GBVH(BVH &bvh){
	GBVH gpu_BVH;


	for(size_t i = 0; i < bvh.nodes.size(); ++i){
		
		gpu_BVH.nodes.push_back(
			BNode(
				vec3(bvh.nodes[i].pmin.x, bvh.nodes[i].pmin.y, bvh.nodes[i].pmin.z), 
				vec3(bvh.nodes[i].pmax.x, bvh.nodes[i].pmax.y, bvh.nodes[i].pmax.z), 
				bvh.nodes[i].left, 
				bvh.nodes[i].right
				)
		);
	}

	for(size_t i = 0; i < bvh.triangles.size(); ++i){
		gpu_BVH.triangles.push_back(BTriangle(
			vec3(bvh.triangles[i].p.x, bvh.triangles[i].p.y, bvh.triangles[i].p.z),
			vec3(bvh.triangles[i].e1.x, bvh.triangles[i].e1.y, bvh.triangles[i].e1.z),
			vec3(bvh.triangles[i].e2.x, bvh.triangles[i].e2.y, bvh.triangles[i].e2.z),
			bvh.triangles[i].id
		));
	}
	
	return gpu_BVH;
} 

/** Get the normal of the triangle hit */
Vector normal(const BHit& hit, const TriangleData& triangle){ 
	return normalize((1 - hit.u - hit.v) * 
	Vector(triangle.na) + hit.u * 
	Vector(triangle.nb) + hit.v * 
	Vector(triangle.nc)); }


Point point(const BHit& hit, const TriangleData& triangle){ return (1 - hit.u - hit.v) * Point(triangle.a) + hit.u * Point(triangle.b) + hit.v * Point(triangle.c); }


Point point(const BHit& hit, const BRay& ray){
	glsl::gvec3<float> v(hit.t * ray.d.x, hit.t * ray.d.y, hit.t * ray.d.z); 
	return Point(ray.o.x + v.x, ray.o.y + v.y, ray.o.z + v.z); 
}

/** Get the normal of the triangle hit */
Vector normal(const Hit& hit, const TriangleData& triangle){ return normalize((1 - hit.u - hit.v) * Vector(triangle.na) + hit.u * Vector(triangle.nb) + hit.v * Vector(triangle.nc)); }

/** Get a point from an hit in a triangle */
Point point(const Hit& hit, const TriangleData& triangle){ return (1 - hit.u - hit.v) * Point(triangle.a) + hit.u * Point(triangle.b) + hit.v * Point(triangle.c); }

/** Get a point from an hit and its ray */
Point point(const Hit& hit, const Ray& ray){ return ray.o + hit.t * ray.d; }

