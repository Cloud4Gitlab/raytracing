#include <cfloat>
#include <random>
#include <chrono>
#include <omp.h>
#include <algorithm>

#include "Global_illumintation.h" 
#include "GPU_Tree.h"
#include "main_gpu_rayTracing.h"

/** 
	Raytracing using the CPU
*/
void cpu_tracing(BVH &bvh, Orbiter &camera, Image &image, Mesh &mesh) {
	std::cout << "omp thread max " << omp_get_max_threads() << std::endl;
	auto cpu_start = std::chrono::high_resolution_clock::now();

	// Loop pre-defined parameters //
	float f_r = (1.0f / (float)DIR_NUMBER) * (1 / M_PI);
	const float scale = 10;

	Point o = camera.position();  // origin			
	Point d1;
	Vector dx1, dy1;

	camera.frame(image.width(), image.height(), 1, 45, d1, dx1, dy1);

	// parcourir tous les pixels de l'image
	// en parallele avec openMP, un thread par bloc de 16 lignes
	#pragma omp parallel for schedule(dynamic, 16) num_threads(omp_get_max_threads())
	for (int py = 0; py < image.height(); ++py) {

		// nombres aleatoires, version c++11
		std::random_device seed;
		// un generateur par thread... pas de synchronisation
		std::mt19937 rng(seed());
		std::uniform_real_distribution<float> u01(0.f, 1.f);

		float y = py + .5f; // centre du pixel en y

		for (int px = 0; px < image.width(); ++px) {
			// generer le rayon pour le pixel (x, y)
			float x = px + .5f; // centre du pixel en x

			Point e = d1 + x * dx1 + y * dy1;  // extremite, see the desc of the frame function in orbiter, do not use if using demi-droite

			// calculer les intersections 
			Ray ray(o, e);

			if (Hit hit = bvh.intersect(ray)) {
				
				// recupere les donnees sur l'intersection
				TriangleData triangle = mesh.triangle(hit.triangle_id);
				Point	p = point(hit, ray);       // point d'intersection
				Vector	pn = normal(hit, triangle); // normale interpolee du triangle au point d'intersection
				if (dot(pn, ray.d) > 0)              // retourne la normale vers l'origine du rayon
					pn = -pn;

				// genere des directions autour de p :
				//Uniform
				//UniformDirection directions(DIR_NUMBER, pn);
				//fibonacci
				fifi directions(DIR_NUMBER, pn, u01(rng));// or uniform : UniformDirection directions(DIR_NUMBER, pn);

				float sumLight = 0;
				Point pn001 = p + pn * .001f;

				for (int i = 0; i < DIR_NUMBER; ++i) { //DIR_NUMBER == directions.size()
					// genere une direction 

					//perturbation de fibonacci
					Vector w(directions(i)); // or simple fibo : Vector w = directions(i); or Direction uniform eq34 : Vector w = directions(u01(rng), u01(rng));
					w = normalize(w);

					// teste le rayon dans cette direction
					Ray shadow(pn001, p + w * scale);

					//quand il y a un hit, il rebondi sur N direction, et donc il faut la moyenne
					if (bvh.visible(shadow)) {// v = 1

						float theta = fmax(dot(pn, w), 0);
						float pdf = directions.pdf(w);//intensit�

						sumLight += (theta / pdf);
					}
					
				}
				sumLight = f_r * sumLight;

				image(px, py) = Color(mesh.triangle_material(hit.triangle_id).diffuse * sumLight /* 20.0f*/, 1);
			}
		}
	}

	auto cpu_stop = std::chrono::high_resolution_clock::now();
	int cpu_time = std::chrono::duration_cast<std::chrono::milliseconds>(cpu_stop - cpu_start).count();
	printf("cpu  %ds %03dms\n", int(cpu_time / 1000), int(cpu_time % 1000));
	
	// SAVING IMAGE SECTION
	std::cout << "fin du calcul, debut d'enregistrement :" << std::endl;

	std::string str = "renderFiboPertu";
	str.append(std::to_string(DIR_NUMBER));
	
	std::string str_hdr(str);
	str.append(".png");
	str_hdr.append(".hdr");
	
	write_image(image, str.c_str());
	write_image_hdr(image, str_hdr.c_str()); // � lire dans l'appli fais par le proff de hdr viewer (pas celui en ligne gros)
	
	int dd;
	std::cout << "fin de l'enregistrement" << std::endl;
	std::cin >> dd;

}


// GPU SECTION

/** */
int main_gpu(BVH &bvh, Orbiter &camera, Mesh &mesh){

	//Step to GPU raytracing
	//Step 0 transform BVH classic to bvh GPU
	GBVH gpu_bvh = transform_bvh_to_GBVH(bvh);

	Raytrace_Compute rc(gpu_bvh, camera, mesh);
	rc.run();
	rc.save_frame();
	return 1;
}


int main(const int argc, const char **argv){
	int GPU;
	const char *mesh_filename = "data/cornell.obj";
	const char *orbiter_filename = "data/cornell_orbiter.txt";
	
	if (argc > 1) GPU = atoi(argv[1]);
	if (argc > 2) mesh_filename = argv[2];
	if (argc > 3) orbiter_filename = argv[3];

	printf("%s: '%s' '%s' GPU=%d\n", argv[0], mesh_filename, orbiter_filename, GPU);

	// creer l'image resultat
	Image image(1024, 640);

	// charger un objet
	Mesh mesh = read_mesh(mesh_filename);
	if (mesh.triangle_count() == 0)
		// erreur de chargement, pas de triangles
		return 1;

	// construire le bvh ou recuperer l'ensemble de triangles du mesh...
	BVH bvh;
	bvh.build(mesh);

	// charger la camera
	Orbiter camera;

	if (camera.read_orbiter(orbiter_filename)) {
		// erreur, pas de camera
		std::cout << "Erreur pas de camera" << std::endl;
		int ext;
		std::cin >> ext;
		return 1;
	}
	
	if(GPU)
		main_gpu(bvh, camera, mesh);
	else
		cpu_tracing(bvh, camera, image, mesh);


	return 0;
}


// Quelques notes :

/*
// calculer l'eclairage ambient :
// en partant de la formulation de l'eclairage direct :
// L_r(p, o) = \int L_i(p, w) * f_r(p, w -> o) * cos \theta dw
// avec
// L_i(p, w) = L_e(hit(p, w), -w) * V(hit(p, w), p)
// L_e(q, v) = 1
// donc L_i(p, w)= V(hit(p, w), p)
// f_r() = 1 / pi

// donc
// L_r(p, w)= \int 1/pi * V(hit(p, w), p) cos \theta dw

// et l'estimateur monte carlo s'ecrit :
// L_r(p, w)= 1/n \sum { 1/pi * V(hit(p, w_i), p) * cos \theta_i } / { pdf(w_i) }

//vec3 l_r, l_i, l_e;
*/