#version 430

#ifdef COMPUTE_SHADER

#define FLT_MAX 3.402823466e+38 // pour GPU

struct Triangle{
	int id; 	//triangle id
	vec3 p; 	//sommet, 
	vec3 e1;	// arete ab
	vec3 e2; 	// arete ac
}; //octets 48

struct Ray{
	float tmax;
	vec3 o;
	vec3 d;
};

struct Hit{
	int triangle_id;
	float t;
	float u, v;
};

layout(binding=0, std430) readonly buffer triangleData{
	Triangle triangles[];
};

layout(binding=1, std430) buffer rayData{
	Ray rays[];
};

layout(binding=2, std430) buffer hitData{
	Hit hits[]; //output
};

uniform int N;//Ray / hit count
uniform int triangles_count;

//algo testé et fonctionnel en C++
bool intersect(const Ray ray, const Triangle tri, Hit hot, out Hit hit){
	hit = hot;

	vec3 pvec = cross(ray.d, tri.e2);
    float det = dot(tri.e1, pvec);
        
    float inv_det = 1 / det;
    vec3 tvec = ray.o - tri.p;

    float u= dot(tvec, pvec) * inv_det;
    if(u < 0 || u > 1) { return false; }

    vec3 qvec= cross(tvec, tri.e1);
    float v= dot(ray.d, qvec) * inv_det;
    if(v < 0 || u + v > 1){ return false; }

    float t= dot(tri.e2, qvec) * inv_det;
    if(t > hot.t || t < 0){ return false; }

	hit.t = t;
	hit.u = u;
	hit.v = v;
	hit.triangle_id = tri.id;

	return true;
}

layout(local_size_x = 1024) in;
void main(){
	uint id = gl_GlobalInvocationID.x;
	
	if(id < N){ 
		hits[id].triangle_id = -1;
		hits[id].t = FLT_MAX;
		hits[id].u = 0;
		hits[id].v = 0;
		Hit hot = hits[id];
		for(int i = 0; i < triangles_count; i++){
			if(intersect(rays[id], triangles[i], hot , hits[id])){
				if(hits[id].triangle_id >= triangles_count){
					hits[id].triangle_id = i;
				}
				hot = hits[id];
			}
		}
	}
		
}


#endif
 